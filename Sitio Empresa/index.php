<?php

//phpinfo();
session_start();

$host = "localhost";
$dbname = "mysql";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo $e->getMessage();
}

function create_database() {
    global $conn;

    $sql_statement = "CREATE DATABASE IF NOT EXISTS empresa;";

    $conn->exec($sql_statement);
}

function create_clientes_table() {
    global $conn;

    $sql_statement = "CREATE TABLE IF NOT EXISTS empresa.Clientes (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(40) NOT NULL,
    apellido VARCHAR(40) NOT NULL,
    usuario VARCHAR(30) NOT NULL,
    contrasena VARCHAR(40) NOT NULL,
    email VARCHAR(50) NOT NULL,
    fecha_de_nacimiento DATE NOT NULL,
    sexo  VARCHAR(1) NOT NULL,
    sueldo DECIMAL(10, 0),
    fecha_de_registro DATETIME NOT NULL,
    CONSTRAINT unicos UNIQUE (usuario, email)
    );";

    $conn->exec($sql_statement);
}

function create_productos_table() {
    global $conn;

    $sql_statement = "CREATE TABLE IF NOT EXISTS empresa.Productos (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(40) NOT NULL,
    marca VARCHAR(40) NOT NULL,
    cantidad INT(6) UNSIGNED NOT NULL,
    precio DECIMAL(10, 0) NOT NULL,
    fecha_de_ingreso DATETIME NOT NULL
    );";

    $conn->exec($sql_statement);
}

function create_compras_table() {
    global $conn;

    $sql_statement = "CREATE TABLE IF NOT EXISTS empresa.Compras (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(40) NOT NULL,
    marca VARCHAR(40) NOT NULL,
    cantidad INT(6) UNSIGNED NOT NULL,
    precio DECIMAL(10, 0) NOT NULL,
    fecha_de_compra DATETIME NOT NULL,
    cliente_id INT(6) UNSIGNED NOT NULL,
    FOREIGN KEY (cliente_id) REFERENCES Clientes(id)
    );";

    $conn->exec($sql_statement);
}

function drop_table() {
    global $conn;

    $sql_statement = "DROP TABLE IF EXISTS empresa.Clientes;";

    $conn->exec($sql_statement);
}

function select($usuario, $contrasena) {
    global $conn;

    $sql_statement = "SELECT id FROM empresa.Clientes WHERE (usuario=:usuario OR email=:usuario) AND contrasena=:contrasena;";
    $prepared_statement = $conn->prepare($sql_statement);
    $prepared_statement->execute(array(':usuario' => $usuario, ':contrasena' => $contrasena));
    $row = $prepared_statement->fetch(); // Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator
    return $row;
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$usuario = $contrasena = "";
$usuario_error = $contrasena_error = $login_error = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['iniciar_sesion'])) {

  if (empty($_POST["usuario"])) {
    $usuario_error = "Ingrese su usuario por favor.";
  } else {
    $usuario = test_input($_POST["usuario"]);
  }

  if (empty($_POST["contrasena"])) {
    $contrasena_error = "Ingrese su contrasena por favor.";
  } else {
    $contrasena = test_input($_POST["contrasena"]);
  }

  if (!empty($_POST["usuario"]) and !empty($_POST["contrasena"])) {
    if (empty(select($usuario, $contrasena))) {
        $login_error = "Nombre de usuario y/o contrasena incorrectos.";
    } else {
        $_SESSION["usuario"] = $usuario;
        header("Location: http://localhost/index.php");
    }
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['cerrar_sesion'])) {
  $_SESSION["usuario"] = "";
  header("Location: http://localhost/index.php");
}

?>


<!DOCTYPE html>
<html>

<head>

<title>Empresa</title>
<link rel="stylesheet" type="text/css" href="estilos.css">

</head>


<body>

<div class="container" class="center">
    <ul>
      <li><a href="index.php">Inicio</a></li>
      <li><a href="#about">Acerca de nosotros</a></li>
    </ul>

    <div <?php if (!empty($_SESSION["usuario"])) echo "hidden"; ?> class="core" >
        <h3>Empresa</h3>
        <h3>Login:</h3>
        <div class="form_container">
            <form method="post" action="index.php">
              Email o Usuario: <input type="text" name="usuario" value="<?php echo $usuario;?>">
              <span class="error"><?php echo $usuario_error;?></span>
              <br>
              Contrasena: <input type="password" name="contrasena" >
              <span class="error"><?php echo $contrasena_error;?></span>
              <br><br>
              <input type="submit" name="iniciar_sesion" value="Iniciar Sesion">
            </form>
            <br>
        </div>
        <span class="error"><?php echo $login_error;?></span>
        <br><br>
        Si aun no se encuentra registrado haga click <a href="registro.php">aqui</a> para registrarse.
        <br>
    </div>

    <div <?php if (empty($_SESSION["usuario"])) echo "hidden"; ?>  class="core" >
        Usted ha iniciado sesion como <?php echo $_SESSION['usuario'] . "."; ?>
        <form method="post" action="index.php">
              <input id="cerrar_sesion" type="submit" name="cerrar_sesion" value="Cerrar Sesion">
        </form>

        <div>
        <br>
          <a href="ver_productos.php">Ver productos</a>
          <a href="ingresar_productos.php">Ingresar productos</a>
        </div>
    </div>
</div>

</body>

</html>
