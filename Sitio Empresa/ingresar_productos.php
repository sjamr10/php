<?php

//phpinfo();
session_start();

$host = "localhost";
$dbname = "mysql";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo $e->getMessage();
}

function insert($nombre, $marca, $cantidad, $precio, $fecha_de_ingreso) {
    global $conn;

    $sql_statement = "INSERT INTO empresa.Productos (nombre, marca, cantidad, precio, fecha_de_ingreso) VALUES
    ('$nombre', '$marca', '$cantidad', '$precio', '$fecha_de_ingreso');";
    $conn->exec($sql_statement);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$usuario = $contrasena = $nombre = $marca = $cantidad = $precio = $fecha_de_ingreso = "";
$usuario_error = $contrasena_error = $login_error = $nombre_error = $marca_error = $cantidad_error = $precio_error = $fecha_de_ingreso_error = $resultado = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['ingresar_producto'])) {
  $datos_son_validos = true;

  if (empty($_POST["nombre"])) {
    $nombre_error = "Ingrese el nombre por favor.";
    $datos_son_validos = false;
  } else {
    $nombre = test_input($_POST["nombre"]);
  }

  if (empty($_POST["marca"])) {
    $marca_error = "Ingrese la marca por favor.";
    $datos_son_validos = false;
  } else {
    $marca = test_input($_POST["marca"]);
  }

  if (empty($_POST["cantidad"])) {
    $cantidad_error = "Ingrese la cantidad por favor.";
    $datos_son_validos = false;
  } else {
    $cantidad = test_input($_POST["cantidad"]);
    # Check if your variable is an integer
    if (!filter_var($cantidad, FILTER_VALIDATE_INT)) {
      $cantidad_error = "Ingrese la cantidad como un numero entero por favor.";;
      $datos_son_validos = false;
    }
  }

    if (empty($_POST["precio"])) {
    $precio_error = "Ingrese el precio por favor.";
    $datos_son_validos = false;
  } else {
    $precio = test_input($_POST["precio"]);
    if (!filter_var($precio, FILTER_VALIDATE_INT)) {
      $precio_error = "Ingrese el precio como un numero entero por favor.";
      $datos_son_validos = false;
    }
  }

  if (empty($_POST["fecha_de_ingreso"])) {
    $fecha_de_ingreso_error = "Ingrese la fecha de ingreso por favor";
    $datos_son_validos = false;
  } else {
    $fecha_de_ingreso = test_input($_POST["fecha_de_ingreso"]);
    // check if name is well-formed
    list($y, $m, $d) = explode('-', $fecha_de_ingreso);

    if (!checkdate($m, $d, $y)) {
      /*
       is valid
      */
       $fecha_de_ingreso_error = "Por favor ingrese la fecha de ingreso. Value: $fecha_de_ingreso";
       $datos_son_validos = false;
    } else {
      $datos_son_validos = true;
    }
  }

  if ($datos_son_validos) {
      insert($nombre, $marca, $cantidad, $precio, $fecha_de_ingreso);
      $resultado = "Producto ingresado exitosamente.";

    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['cerrar_sesion'])) {
  $_SESSION["usuario"] = "";
  header("Location: http://localhost/index.php");
}

?>


<!DOCTYPE html>
<html>

<head>

<title>Empresa</title>
<link rel="stylesheet" type="text/css" href="estilos.css">

</head>

<body>

<div class="container" class="center">

    <ul>
      <li><a href="index.php">Inicio</a></li>
      <li><a href="#about">Acerca de nosotros</a></li>
    </ul>

    <div <?php if (empty($_SESSION["usuario"])) echo "hidden"; ?>  class="core" >
        Usted ha iniciado sesion como <?php echo $_SESSION['usuario'] . "."; ?>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
              <input id="cerrar_sesion" type="submit" name="cerrar_sesion" value="Cerrar Sesion">
        </form>
        <br>
        <div>
          <a href="ver_productos.php">Ver productos</a>
          <a href="ingresar_productos.php">Ingresar productos</a>
        </div>
        <br>
        <div class="form_container">
            <form id='ingresar_form' method="post" action="ingresar_productos.php">
              <div>
              Nombre: <input class='ingresar_input' type="text" name="nombre" value="<?php echo $nombre; ?>">
              <span class="error"><?php echo $nombre_error; ?></span>
              </div>
              <div>
              Marca: <input class='ingresar_input' type="text" name="marca" value="<?php echo $marca; ?>">
              <span class="error"><?php echo $marca_error; ?></span>
              </div>
              <div>
              Cantidad: <input class='ingresar_input' type="text" name="cantidad" value="<?php echo $cantidad; ?>">
              <span class="error"><?php echo $cantidad_error; ?></span>
              </div>
              <div>
              Precio: <input class='ingresar_input' type="text" name="precio" value="<?php echo $precio; ?>">
              <span class="error"><?php echo $precio_error; ?></span>
              </div>
              <div>
              <select name='marca' form='ingresar_form'>
                <option value="volvo">Volvo</option>
                <option value="saab">Saab</option>
                <option value="mercedes">Mercedes</option>
                <option value="audi">Audi</option>
              </select>
              </div>
              <?php
              $now = new DateTime();
              $now->setTimezone(new DateTimeZone('America/Santiago'));
              ?>
              Fecha de ingreso: <input class='ingresar_input' type="date" name="fecha_de_ingreso" max='<?php echo $now->format('Y-m-d'); ?>' min="1900-01-01" value="<?php echo $fecha_de_ingreso; ?>">
              <span class="error"><?php echo $fecha_de_ingreso_error; ?></span>
              <br>
              <br>
              <div>
              <input type="submit" name="ingresar_producto" value="Ingresar Producto">
              <br><br>
              <input type="reset">
              </div>
              <span class="error"><?php echo "<h3>$resultado</h3>"; ?></span><br>
            </form>
        </div>
    </div>

</div>

</body>

</html>
