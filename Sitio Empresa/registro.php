<?php

//phpinfo();
session_start();

$host = "localhost";
$dbname = "mysql";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo $e->getMessage();
}

function select($usuario, $contrasena) {
  global $conn;

  $sql_statement = "SELECT id FROM empresa.Clientes WHERE (usuario=:usuario OR email=:usuario) AND contrasena=:contrasena;";
  $prepared_statement = $conn->prepare($sql_statement);
  $prepared_statement->execute(array(':usuario' => $usuario, ':contrasena' => $contrasena));
  $row = $prepared_statement->fetch(); // Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator
  return $row;
}

function ya_existe_usuario($usuario) {
  global $conn;

  $sql_statement = "SELECT id FROM empresa.Clientes WHERE usuario=:usuario;";
  $prepared_statement = $conn->prepare($sql_statement);
  $prepared_statement->execute(array(':usuario' => $usuario));
  $row = $prepared_statement->fetch(); // Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator
  if (empty($row)) {
    return false;
  } else {
    return true;
  }
}

function ya_existe_email($email) {
  global $conn;

  $sql_statement = "SELECT id FROM empresa.Clientes WHERE email=:email;";
  $prepared_statement = $conn->prepare($sql_statement);
  $prepared_statement->execute(array(':email' => $email));
  $row = $prepared_statement->fetch(); // Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator
  if (empty($row)) {
    return false;
  } else {
    return true;
  }
}

function insert($nombre, $apellido, $usuario, $contrasena, $email, $fecha_de_nacimiento, $sexo, $fecha_de_registro) {
    global $conn;

    $sql_statement = "INSERT INTO empresa.Clientes (nombre, apellido, usuario, contrasena, email, fecha_de_nacimiento, sexo, fecha_de_registro) VALUES
    ('$nombre', '$apellido', '$usuario', '$contrasena', '$email', '$fecha_de_nacimiento', '$sexo', '$fecha_de_registro');";
    $conn->exec($sql_statement);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// define variables and set to empty values
$usuario_error = $nombre_error = $apellido_error = $contrasena_error = $confirmar_contrasena_error = $email_error = $fecha_de_nacimiento_error = $sexo_error = $problema_datos = "";
$usuario = $nombre = $apellido = $contrasena = $confirmar_contrasena = $email = $fecha_de_nacimiento = $sexo =  "";


if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['registrarse'])) {
  $datos_son_validos = true;

  if (empty($_POST["usuario"])) {
    $usuario_error = "Ingrese su usuario por favor";
    $datos_son_validos = false;
  } else {
    $usuario = test_input($_POST["usuario"]);
    $datos_son_validos = true;
  }

  if(!empty($_POST["contrasena"]) && ($_POST["contrasena"] == $_POST["confirmar_contrasena"])) {
      $contrasena = test_input($_POST["contrasena"]);
      $confirmar_contrasena = test_input($_POST["confirmar_contrasena"]);
      if (strlen($_POST["contrasena"]) < '8') {
          $contrasena_error = "Tu contrasena debe contener al menos 8 caracteres";
          $datos_son_validos = false;
      }
      else {
        $datos_son_validos = true;
      }
  }
  elseif (empty($_POST["contrasena"])) {
      $confirmar_contrasena_error = "Por favor revise que ingreso su contrasena";
      $datos_son_validos = false;
  }

  if (empty($_POST["nombre"])) {
    $nombre_error = "Ingrese su nombre por favor";
    $datos_son_validos = false;
  } else {
    $nombre = test_input($_POST["nombre"]);
    $datos_son_validos = true;
  }

  if (empty($_POST["apellido"])) {
    $apellido_error = "Ingrese su apellido por favor";
    $datos_son_validos = false;
  } else {
    $apellido = test_input($_POST["apellido"]);
    $datos_son_validos = true;
  }

  if (empty($_POST["email"])) {
    $email_error = "Ingrese su email por favor";
    $datos_son_validos = false;
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Formato de email incorrecto";
      $datos_son_validos = false;
    } else {
      $datos_son_validos = true;
    }
  }

  if (empty($_POST["fecha_de_nacimiento"])) {
    $fecha_de_nacimiento_error = "Ingrese su fecha de nacimiento por favor";
    $datos_son_validos = false;
  } else {
    $fecha_de_nacimiento = test_input($_POST["fecha_de_nacimiento"]);
    // check if name is well-formed
    list($y, $m, $d) = explode('-', $fecha_de_nacimiento);

    if (!checkdate($m, $d, $y)) {
       $fecha_de_nacimiento_error = "Por favor ingrese su fecha de nacimiento. Value: $fecha_de_nacimiento";
       $datos_son_validos = false;
    } else {
      $datos_son_validos = true;
    }
  }

  if (empty($_POST["sexo"])) {
    $sexo_error = "Seleccione su sexo por favor";
    $datos_son_validos = false;
  } else {
    $sexo = test_input($_POST["sexo"]);
    $datos_son_validos = true;

  }

  if ($datos_son_validos) {
    if (ya_existe_usuario($usuario)) {
      $problema_datos = "Ya existe una cuenta registrada asociada a ese nombre de usuario, por favor escoger otro nombre.";
    } elseif (ya_existe_email($email)) {
      $problema_datos = "Ya existe una cuenta registrada asociada a ese email, por favor escoger otro email.";
    } else {
      $problema_datos = "";

      $now = new DateTime();
      $now->setTimezone(new DateTimeZone('America/Santiago'));
      $fecha_de_registro = $now->format('Y-m-d H:i:s');

      insert($nombre, $apellido, $usuario, $contrasena, $email, $fecha_de_nacimiento, $sexo, $fecha_de_registro);
      $_SESSION["usuario"] = $usuario;
      header("Location: http://localhost/index.php");
    }

  }
}

?>


<!DOCTYPE html>
<html>

<head>

<title>Empresa</title>
<link rel="stylesheet" type="text/css" href="estilos.css">

</head>


<body>

<div class="container">

    <ul>
      <li><a href="index.php">Inicio</a></li>
      <li><a href="#about">Acerca de nosotros</a></li>
    </ul>

    <div class="core">
        <div hidden id="debug">
        <?php echo $fecha_de_registro; ?>
        </div>
        <h4>Formulario Registro</h4>
        <div class="form_container">
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
              <div>
              Nombre: <input type="text" name="nombre" value="<?php echo $nombre; ?>">
              <span class="error"><?php echo $nombre_error; ?></span>
              </div>
              <div>
              Apellido: <input type="text" name="apellido" value="<?php echo $apellido; ?>">
              <span class="error"><?php echo $apellido_error; ?></span>
              </div>
              <div>
              Usuario: <input type="text" name="usuario" value="<?php echo $usuario; ?>">
              <span class="error"><?php echo $usuario_error;?></span>
              </div>
              <div>
              Contrasena: <input type="password" name="contrasena" value="<?php echo $contrasena; ?>">
              <span class="error"><?php echo $contrasena_error; ?></span>
              </div>
              <div>
              Confirmar Contrasena: <input type="password" name="confirmar_contrasena" value="<?php echo $confirmar_contrasena; ?>">
              <span class="error"><?php echo $confirmar_contrasena_error; ?></span>
              </div>
              <div>
              E-mail: <input type="text" name="email" value="<?php echo $email; ?>">
              <span class="error"><?php echo $email_error; ?></span>
              </div>
              Fecha de nacimiento: <input type="date" name="fecha_de_nacimiento" max="2016-10-10" min="1900-01-01" value="<?php echo $fecha_de_nacimiento; ?>">
              <span class="error"><?php echo $fecha_de_nacimiento_error; ?></span>
              <div>
              Sexo: <span class="error"><?php echo $sexo_error; ?></span><br>
              Femenino
              <input type="radio" name="sexo" <?php if (isset($sexo) && $sexo=="f") echo "checked"; ?> value="f">
              Masculino
              <input type="radio" name="sexo" <?php if (isset($sexo) && $sexo=="m") echo "checked"; ?> value="m">
              </div>
              <br>
              <div>
              <input type="submit" name="registrarse" value="Registrarse">
              <br><br>
              <input type="reset">
              </div>
              <span class="error"><?php echo "<h3>$problema_datos</h3>"; ?></span><br>
            </form>
        </div>
    </div>

    <br>
</div>

</body>

</html>
