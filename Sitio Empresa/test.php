<!DOCTYPE html>
<html>

<head>

<title>Empresa</title>

</head>

<body>

<h3>Empresa</h3>
<p>This is a paragraph.</p>

<?php //phpinfo();

function function1() {
    $x = 5985;
    var_dump($x);
    $x = 10.365;
    var_dump($x);
    $name = "Sergio Miranda";
    echo "$name";
    var_dump($name);
    $amigos = array('Eduardo', 'Andres', 'Miguel');
    $amigos[] = 'Jaime';
    var_dump($amigos);
    $x = null;
    var_dump($x);
    $book = array('author' => 'David Powers',
                  'title' => '"Adobe Dreamweaver CS5 with PHP"');
    $book['publisher'] = 'Adobe Press';
    class Car {
        function Car() {
            $this->model = "VW";
        }
    }

    // create an object
    $auto = new Car();

    // show object properties
    echo "$auto->model";
    echo "<br>";

    echo strlen("Hello world!"); // outputs 12
    echo "<br>";
    echo str_word_count("Hello world!"); // outputs 2
    echo "<br>";
    echo strrev("Hello world!"); // outputs !dlrow olleH
    echo "<br>";
    echo strpos("Hello world!", "world"); // outputs 6
    echo "<br>";
    echo str_replace("world", "Dolly", "Hello world!"); // outputs Hello Dolly!
    echo "<br>";
    define("GREETING", "Welcome to W3Schools.com!");
    echo GREETING;
    echo "<br>";

}


function function2() {
    $now = new DateTime();
    $now->setTimezone(new DateTimeZone('America/Santiago'));
    echo $now->format('Y-m-d H:i:s');
    echo "<br>";

    $t = $now->format('H');

    if ($t < "10") {
        echo "Have a good morning!";
    } elseif ($t < "20") {
        echo "Have a good day!";
    } else {
        echo "Have a good night!";
    }
    echo "<br>";

    $favcolor = "red";

    switch ($favcolor) {
        case "red":
            echo "Your favorite color is red!";
            break;
        case "blue":
            echo "Your favorite color is blue!";
            break;
        case "green":
            echo "Your favorite color is green!";
            break;
        default:
            echo "Your favorite color is neither red, blue, nor green!";
    }

    echo "<br>";

    $x = 1;

    while ($x <= 5) {
        echo "The number is: $x <br>";
        $x++;
    }

    echo "<br>";

    for ($x = 0; $x <= 10; $x++) {
        echo "The number is: $x <br>";
    }

    echo "<br>";

    $colors = array("red", "green", "blue", "yellow");

    foreach ($colors as $value) {
        echo "$value <br>";
    }

    echo "<br>";

    function writeMsg() {
        echo "Hello world!<br>";
    }

    writeMsg(); // call the function
}


function function3() {
    echo "<br>";

    function familyName($fname) {
        echo "$fname Refsnes.<br>";
    }

    familyName("Jani");
    familyName("Hege");
    familyName("Stale");
    familyName("Kai Jim");
    familyName("Borge");

    echo "<br>";

    function sum($x, $y) {
        $z = $x + $y;
        return $z;
    }

    echo "5 + 10 = " . sum(5, 10) . "<br>";
    echo "7 + 13 = " . sum(7, 13) . "<br>";
    echo "2 + 4 = " . sum(2, 4) . "<br>";

    echo "<br>";

    $cars = array("Volvo", "BMW", "Toyota");
    echo count($cars);

    echo "<br>";

    $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

    foreach($age as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }

    echo "<br>";

    $cars = array
      (
      array("Volvo",22,18),
      array("BMW",15,13),
      array("Saab",5,2),
      array("Land Rover",17,15)
      );

}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $name = $_POST['name'];
    if (empty($name)) {
        echo "Name is empty";
    } else {
        echo "Name: $name";
    }
}

echo "<br>";


?>


<br><br>
<form action="<?php echo  htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    Nombre: <input type="text" name="name"><br><br>
    E-mail: <input type="text" name="email"><br><br>
    Website: <input type="text" name="website"><br><br>
    Comentario: <textarea name="comment" rows="5" cols="40"></textarea><br><br>
    Sexo:<br>
    <input type="radio" name="gender" value="female">Femenino<br>
    <input type="radio" name="gender" value="male">Masculino<br><br>
    <input type="submit"><br>
</form>



<br>


<?php

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "mysql";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $create_database_statement = "CREATE DATABASE IF NOT EXISTS empresa";

    // sql to create table
    $create_table_statement = "CREATE TABLE IF NOT EXISTS empresa.Clientes (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    fecha_de_registro TIMESTAMP
    )";

    $delete_table_statement = "DROP TABLE empresa.Clientes";

    $sql_statement = $create_table_statement;
    // use exec() because no results are returned
    $conn->exec($sql_statement);
    echo "Statement executed successfully.<br>";
    }
catch(PDOException $e)
    {
    echo $sql_statement . "<br>" . $e->getMessage();
    }

?>


</body>

</html>
