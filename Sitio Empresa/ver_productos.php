<?php

//phpinfo();
session_start();

$host = "localhost";
$dbname = "mysql";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo $e->getMessage();
}

function select() {
    global $conn;

    $sql_statement = "SELECT * FROM empresa.Productos;";
    $prepared_statement = $conn->prepare($sql_statement);
    $prepared_statement->execute();
    $rows = $prepared_statement->fetchAll(PDO::FETCH_ASSOC); // Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator
    return $rows;
}

function update($id, $nombre, $marca, $cantidad, $precio, $fecha_de_ingreso) {
    global $conn;

    $sql_statement = "UPDATE empresa.Productos SET nombre='$nombre', marca='$marca', cantidad='$cantidad', precio='$precio', fecha_de_ingreso='$fecha_de_ingreso' WHERE id=$id;";
    $conn->exec($sql_statement);
}

function delete($id) {
    global $conn;

    $sql_statement = "DELETE FROM empresa.Productos WHERE id=$id;";
    $conn->exec($sql_statement);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

$usuario_error = $contrasena_error = $login_error = $error = $resultado = "";
$usuario = $contrasena = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['modificar'])) {
  $datos_son_validos = true;

  $id = test_input($_POST["id"]);

  if (empty($_POST["nombre"])) {
    $error = "Ingrese el nombre por favor.";
    $datos_son_validos = false;
  } else {
    $nombre = test_input($_POST["nombre"]);
  }

  if (empty($_POST["marca"])) {
    $error = "Ingrese la marca por favor.";
    $datos_son_validos = false;
  } else {
    $marca = test_input($_POST["marca"]);
  }

  if (empty($_POST["cantidad"])) {
    $error = "Ingrese la cantidad por favor.";
    $datos_son_validos = false;
  } else {
    $cantidad = test_input($_POST["cantidad"]);
    # Check if your variable is an integer
    if (!filter_var($cantidad, FILTER_VALIDATE_INT)) {
      $error = "Ingrese la cantidad como un numero entero por favor.";;
      $datos_son_validos = false;
    }
  }

    if (empty($_POST["precio"])) {
    $error = "Ingrese el precio por favor.";
    $datos_son_validos = false;
  } else {
    $precio = test_input($_POST["precio"]);
    if (!filter_var($precio, FILTER_VALIDATE_INT)) {
      $error = "Ingrese el precio como un numero entero por favor.";
      $datos_son_validos = false;
    }
  }

  if (empty($_POST["fecha_de_ingreso"])) {
    $error = "Ingrese la fecha de ingreso por favor";
    $datos_son_validos = false;
  } else {
    $fecha_de_ingreso = test_input($_POST["fecha_de_ingreso"]);
    // check if name is well-formed
    list($y, $m, $d) = explode('-', $fecha_de_ingreso);

    if (!checkdate($m, $d, $y)) {
      /*
       is valid
      */
       $error = "Por favor ingrese la fecha de ingreso. Value: $fecha_de_ingreso";
       $datos_son_validos = false;
    } else {
      $datos_son_validos = true;
    }
  }

  if ($datos_son_validos) {
      update($id, $nombre, $marca, $cantidad, $precio, $fecha_de_ingreso);
      $resultado = "Producto actulizado exitosamente.";
  }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['eliminar'])) {
  $id = test_input($_POST["id"]);

  delete($id);
  $resultado = "Producto eliminado exitosamente.";
}

if ($_SERVER["REQUEST_METHOD"] == "POST" AND isset($_POST['cerrar_sesion'])) {
  $_SESSION["usuario"] = "";
  header("Location: http://localhost/index.php");
}

?>


<!DOCTYPE html>
<html>

<head>

<title>Empresa</title>
<link rel="stylesheet" type="text/css" href="estilos.css">

</head>

<body>

<div class="container" class="center">

    <ul>
      <li><a href="index.php">Inicio</a></li>
      <li><a href="#about">Acerca de nosotros</a></li>
    </ul>

    <div <?php if (empty($_SESSION["usuario"])) echo "hidden"; ?>  class="core" >
        Usted ha iniciado sesion como <?php echo $_SESSION['usuario'] . "."; ?>
        <form method="post" action="ver_productos.php">
              <input id="cerrar_sesion" type="submit" name="cerrar_sesion" value="Cerrar Sesion">
        </form>

        <div>
        <br>
          <a href="ver_productos.php">Ver productos</a>
          <a href="ingresar_productos.php">Ingresar productos</a>
        </div>
        <br>
        <div id='productos_form'>
              <?php

              $rows = select();
              foreach ($rows as $row) {
                 echo "<form method='post' action='ver_productos.php'>";
                 $id = $row['id'];
                 $nombre = $row['nombre'];
                 $marca = $row['marca'];
                 $cantidad = $row['cantidad'];
                 $precio = $row['precio'];
                 $fecha_de_ingreso = $row['fecha_de_ingreso'];
                 echo "<span class='row'>";
                 echo "<input class='ver_input' type='text' name='id' value='$id' hidden>";
                 echo "<input class='ver_input' type='text' name='nombre' value='$nombre'>";
                 echo "<input class='ver_input' type='text' name='marca' value='$marca'>";
                 echo "<input class='ver_input' type='text' name='cantidad' value='$cantidad'>";
                 echo "<input class='ver_input' type='text' name='precio' value='$precio'>";
                 echo  "<input type='date' id='fecha_de_ingreso' name='fecha_de_ingreso' max='";
                 $now = new DateTime();
                 $now->setTimezone(new DateTimeZone('America/Santiago'));
                 echo $now->format('Y-m-d');
                 echo  "' min='1900-01-01' value='";
                 echo substr($fecha_de_ingreso, 0, 10);
                 echo "'>";
                 echo "</span>";
                 echo "<input id='modificar' type='submit' name='modificar' value='Modificar'>";
                 echo "<input id='eliminar' type='submit' name='eliminar' value='Eliminar'>";
                 echo "<br>";
                 echo "<br>";
                 echo "</form>";
              }

              ?>
              <span class="error"><?php echo $error;?></span>
              <span class="error"><?php echo "<h3>$resultado</h3>"; ?></span><br>
        </div>

    </div>

</div>

</body>

</html>
