#!/bin/bash

# script to initialize a server, installing common stuff and the like

# SERVER MACHINE
# iptables -A INPUT -p icmp -j ACCEPT
# ufw allow 22 # allow ssh access
# service ssh start

# now check if can connect through SSH keys
# if can connect then disable root login

create_user() {
  apt-get -y install sudo
  sudo apt-get -y update && sudo apt-get -y upgrade
  sudo apt-get -y install git


  if useradd sjamr10 -s /bin/bash -m -g sudo ; then
    echo "useradd succeeded"
  else
    echo "useradd failed"
    return 1
  fi

  if echo sjamr10:TZRhGyJ7wVwrXxmg | chpasswd; then
    echo "chpasswd succeeded"
  else
    echo "chpasswd failed"
    return 2
  fi

  su - sjamr10

  cd
  git clone git@gitlab.com:sjamr10/php.git php

}


add_ssh() {
  if mkdir ~/.ssh; then
    echo "mkdir ~/.ssh succeded"
  else
    echo "mkdir ~/.ssh failed"
    return 4
  fi

  if chmod 700 ~/.ssh; then
    echo "chmod 700 ~/.ssh succeded"
  else
    echo "chmod 700 ~/.ssh failed"
    return 5
  fi

  add_public_key() {
    public_key=
    while [ -z "${public_key}"  ]
    do
        read -p "Enter public key: " public_key
    done
    echo "${public_key}" >> ~/.ssh/authorized_keys
  }

  if add_public_key; then
    # insert your public key (which should be in your clipboard) by pasting it into the editor.
    echo "add_public_key succeded"
  else
    echo "add_public_key failed"
    return 6
  fi

  if chmod 600 ~/.ssh/authorized_keys; then
    echo "chmod 600 ~/.ssh/authorized_keys succeded"
  else
    echo "chmod 600 ~/.ssh/authorized_keys failed"
    return 7
  fi

  return 0
  # echo "AllowUsers sjamr10" >> /etc/ssh/sshd_config
}





install_stuff() {
  sudo apt-get -y update && sudo apt-get -y upgrade
  sudo apt-get -y install git
  sudo apt-get -y install grep
  sudo apt-get -y install apt-transport-https
  sudo apt-get -y install ca-certificates
  sudo apt-get -y install curl
  sudo apt-get -y install software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get update
  sudo apt-get install docker-ce=17.03.1~ce-0~ubuntu-xenial
  sudo groupadd docker
  sudo usermod -aG docker sjamr10
  sudo apt-get -y install iproute2
  sudo apt-get -y install iputils-ping
  cd
}

clone_stuff() {
  mkdir ~/.ssh
  cd ~/.ssh
  ssh-keygen
  cat ~/.ssh/id_rsa.pub
  echo ""
  echo "Add your public key to gitlab keys."
  read -n1 -r -p "Press space to continue..." key
  cd
  mkdir bash
  cd bash
  git clone git@gitlab.com:sjamr10/bash.git .
  cp .bashrc ~/.bashrc
  . ~/.bashrc
  mkdir compa
}

prevent_root_login() {
  if [!grep -Fxq "PasswordAuthentication no" /etc/ssh/sshd_config]; then
    echo "It was already executed"
    sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthenticatilon no/g' /etc/ssh/sshd_config
    sudo sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config
    sudo systemctl reload sshd
    return 0
  else
    echo "prevent_root_login failed"
    return 1
  fi
}





# FOR DOCKER
# cd /mnt/macOS/Users/sergiomiranda/code/bash/
# chmod +x init_server.sh
# . init_server.sh

# add_ssh

# create_user
# install_stuff_and_clone_bash
# prevent_root_login


# service ssh restart



# FOR SERVER
# sftp user@server
# put init_server.sh
# bye
# ssh user@server

# chmod +x init_server.sh
# ./init_server.sh
